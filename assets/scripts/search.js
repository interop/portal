const searchClient = algoliasearch(ALGOLIA_APP_ID, ALGOLIA_READ_ONLY_API_KEY);

const search = instantsearch({
  indexName: ALGOLIA_INDEX_NAME,
  searchClient
});

search.addWidget(
  instantsearch.widgets.searchBox({
    container: '#uw-search-box',
    placeholder: 'Search dev portal',
    showSubmit: false,
    showReset: false
  })
);

/**
 * By default, Algolia will return all matching results.
 * Cap the number of shown results at `MAX_SHOWN_RESULTS`.
 */
const MAX_SHOWN_RESULTS = 5;

search.addWidget(
  instantsearch.widgets.hits({
    container: '#uw-search-hits',
    templates: {
      empty: 'No results',
      item: '<a href="{{relpermalink}}">{{title}}</a>'
    },
    transformItems: items => items.slice(0, MAX_SHOWN_RESULTS)
  })
);

const searchWrapper = document.querySelector('#uw-search-wrapper');
const dummySearchInput = document.querySelector('#uw-search-input-dummy');

function showHits() {
  console.log('show hits');
  document.querySelector('#uw-search-hits').style.display = 'block';
}

function hideHits() {
  console.log('hide hits');
  document.querySelector('#uw-search-hits').style.display = 'none';
}

document.body.addEventListener('click', function(event) {
  if (searchWrapper.contains(event.target)) {
    showHits();
  } else {
    hideHits();
  }
});

searchWrapper.addEventListener('focusin', function() {
  showHits();
});

function onDummyFocus() {
  console.log('remove dummy');
  this.parentNode.removeChild(this);
  search.start();

  var realSearchInput = document.querySelector(
    '#uw-search-box .ais-SearchBox-input'
  );

  realSearchInput.focus();
}

dummySearchInput.addEventListener('focus', onDummyFocus);
