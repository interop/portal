var headers = document.querySelectorAll('h2,h3,h4,h5,h6');
headers.forEach(function(h) {
  if (h.id) {
    var a = document.createElement('a');
    a.href = '#' + h.id;
    a.setAttribute('aria-hidden', 'true');
    a.classList.add('header-permalink');
    h.prepend(a);
  }
});
