## Developer Portal
* https://developer.wisc.edu/.

## Content Publishing Process
* Content can be written in [markdown](https://daringfireball.net/projects/markdown/).
* Markdown file should be placed inside the [content](./content) directory.
* Each markdown file should start with some meta data, that indicate title of the post, time of the publication and if the post is a draft.

```
---
title: "UW API Design"
date: 2019-11-09T22:23:34-06:00
draft: false
---
```

* After meta data section, write your content on the markdown file.
* If you include images in your markdown files put them into an `images` folder and use following template to refer them in your markdown content file:
```
![Some alt text](./images/some-pic.jpg)
```
* `draft` can be `true` if you don't want to publish your content to developer portal.
* Any change that merge into master branch will be automatically get deployed to developer portal at https://developer.wisc.edu/ (given `draft` is `false`). This also means you can send a PR for review, before publishing the content on developer portal.
* [.gitlab-ci.yml](./.gitlab-ci.yml) for this project generates the HTML content from markdown files and copyies to AWS CodeCommit repository (https://git-codecommit.us-east-1.amazonaws.com/v1/repos/portal) and from AWS CodeCommit changes will be deployed into developer portal (AWS [Amplify](https://aws.amazon.com/amplify/) is used as the hosting platform).
* See existing content inside the [content](./content) directory for examples.

### Ordering Lists
If you need to order content in a specific directory, Hugo uses a few methods to determine the order.
Consult [this documentation](https://gohugo.io/templates/lists/#order-content) to find the best way to order your content.

### Interactively Write Content Locally
The site is generated using [Hugo](https://gohugo.io/); if you'd like to create your content interactively, make sure [Hugo is installed](https://gohugo.io/getting-started/quick-start/#step-1-install-hugo)
and then run:

	hugo server -D

in the project's root directory. This will generate
site content and expose it via a local webserver accessible at [http://localhost:1313](http://localhost:1313).
The server will watch for content changes, so you can edit a document and see the
rendered changes immediately.

If you have Hugo installed, you can also automate the creation of new posts:

	hugo new posts/new-api-manager-features.md

Take a stroll through the [Hugo quickstart](https://gohugo.io/getting-started/quick-start)
for more information on the site creation process.


## AWS Amplify Domin Name Configuration
This is hosted on AWS [Amplify](https://aws.amazon.com/amplify/). The domain name configurations can be found under `App settings` -> `Domain management`. Also any re-write rules can be added under `App settings` -> `Rewrites and redirects`.

## Checking Broken Links
Use [scripts/link-checker.sh](scripts/link-checker.sh) to check all markdown files for broken links.
This will check relative links, links to images, and links to external domains.

### Usage

1. Install [markdown-link-check](https://www.npmjs.com/package/markdown-link-check).
2. Run [scripts/link-checker.sh](scripts/link-checker.sh) from a terminal:
```sh
cd scripts
./link-checker.sh
```
3. If broken links are found, the script will output them and `exit 1`

If you need to exclude links from being checked, edit [scripts/link-checker-config.json](scripts/link-checker-config.json).


## Search

The site uses [Algolia](https://www.algolia.com/) for creating and serving a search index.
When Hugo generates the static site, it creates a file named `algolia.list.json` in the public directory that contains a simplified text-only representation of each document on the site.
When the GitLab runner deploys the site, that JSON file is uploaded to Algolia and its content is indexed.

New documents added to the site will automatically be added to the search index upon merge into the master branch.

### Search credentials and API keys

There are a few pieces of information used when talking to Algolia:

Name | Is it secret? | Where is it set? | What is its purpose?
:------|:------|:------|:------
`ALGOLIA_ADMIN_KEY` | Yes | Within this project's CI/CD variables | To write new documents to the Algolia search index
`ALGOLIA_APP_ID` | No  | As an ENV variable in `.gitlab-ci.yml` | Which Algolia application is submitting queries. This ID is sent to browsers for use in API calls
`ALGOLIA_INDEX_NAME` | No  | As an ENV variable in `.gitlab-ci.yml` | Which search index to query. This name is sent to browsers for use in API calls
`ALGOLIA_READ_ONLY_API_KEY` | No  | As an ENV variable in `.gitlab-ci.yml` | To search data in public indices. This key is sent to browsers for use in API calls

### Using search locally

When developing changes sometimes it's necessary to test the search functionality locally.
In order to configure search locally, set the `ALGOLIA_APP_ID`, `ALGOLIA_INDEX_NAME`, and `ALGOLIA_READ_ONLY_API_KEY` ENV variables as the values listed in the `.gitlab-ci.yml` file when launching the local Hugo server.

For example (replacing the variable values with the values from `.gitlab-ci.yml`):

```bash
ALGOLIA_APP_ID="1234" ALGOLIA_INDEX_NAME="my-index-name" ALGOLIA_READ_ONLY_API_KEY="7890" hugo server
```

**Remember:** when searching locally the search index *not* include any changes that haven't been merged into the master branch.

## Hugo Docker Image
Portal depends on a customized Hugo docker image for CI/CD and it can be found in this [repository](https://git.doit.wisc.edu/interop/hugo). See [.gitlab-ci.yml](./.gitlab-ci.yml) for more details.
