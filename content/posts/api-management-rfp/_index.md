---
title: "Exploring Other API Management Options"
---
2020-04-17
## Summary

- Following the RFP that is underway, WSO2 API Cloud might be replaced by different API management product.
- If so, we would communicate migration plans far in advance and make sure there is operational overlap of the old and new platforms.
- The developer portal is a proof concept. Content, link structure, and layout are subject to be changed.

## Introduction

If you're keeping up with the Interoperability Initiative, you might be aware that we (the Interoperability Program Area within DoIT) completed an RFI in February 2020 for API management, integration platform, and identity and access management (IAM) infrastructure.
For API management, we decided to proceed with an RFP.

As of writing this post, our API management service is supported by WSO2 API Cloud.
API Cloud is WSO2's software-as-a-service (SaaS) product.
It is almost identical to their traditional on-premises product, API Manager, except the underlying infrastructure is managed by WSO2.

## Background

We started implementing WSO2 API Cloud in mid-2019 to replace our on-premises infrastructure that ran WSO2 API Manager.
At the end of December 2019, we were able to decommission API Manager and complete the transition to API Cloud.
Transitioning to a cloud offering of an on-premises system allowed API subscribers, publishers, and us as infrastructure providers, to use a familiar system while taking advantage of the fact that WSO2 was managing the infrastructure for us.

However, the transition wasn't a means to an end.
Moving to the cloud doesn't mean we have solved all our problems.
The RFI and subsequent RFP allows us to look at a the broad range of API management products out there and make sure we are using the right one for our needs as a university.

## Impacts for WSO2 API Cloud Users

We are still providing WSO2 API Cloud as a service for API publishers and subscribers.
New users are welcome, but we want to be transparent about the fact that WSO2 API Cloud might be replaced by a different API management product. If so, we would communicate migration plans far in advance and make sure there is operational overlap of the old and new platforms.

For API publishers, we encourage you to avoid implementing business logic in API Cloud by limiting use of features like [message mediation](https://docs.wso2.com/display/APICloud/Message+Mediation+for+WSO2+API+Cloud).
This would help ease the transition to a different API management platform, should that be necessary.
Reducing the amount of message transformation in the API manager is a practice we recommend in general.
See our [best practices for publishing APIs](../../publish-apis/explanation/best-practices) for more information.

## The Developer Portal

A developer portal allows self-service access to use APIs, but also serves documentation and provides links to other resources that might be useful for developers and less-technically savvy integrators.
WSO2 API Cloud offers their [API Store](https://wso2apistore.services.wisc.edu/) as their developer portal product.

[developer.wisc.edu](https://developer.wisc.edu/) is our current developer portal to help supplement WSO2's API Store with additional documentation.
This developer portal is a proof of concept.
Most API management products also include a developer portal offering, and depending on the outcome of the RFP, this developer portal might be replaced by a vendor-delivered product.

To that end, we encourage you to continue using this developer portal for WSO2 documentation, but know that content, link structure, and overall layout is subject to be changed.

