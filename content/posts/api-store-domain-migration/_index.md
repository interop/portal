---
title: "API Store Domain Name Migration, API Management Updates"
---

## Summary

- The current API Store domain name, `api.wisc.edu`, will be changing to `wso2apistore.services.wisc.edu` on **September 21st, 2021**.
- This change does not affect API calls through `gateway.api.wisc.edu`.
- After this change, `api.wisc.edu` will temporarily resolve to a page instructing users to update their bookmarks. Following that, `api.wisc.edu` will be used as the production domain for handling API calls to the new API Management platform, Google Apigee.

## Background

Following [an RFP that was completed last year for API Management](../api-management-rfp), the Integration Platform Team has recently been working to implement Google Apigee.
APIs will eventually need to be migrated from the current API Management platform, WSO2 API Cloud, to Google Apigee.
When migration plans are better understood, expect to hear more details along with ample time for migration.

Until now, much of the work implementing Apigee has been able to be behind the scenes, but not without constant feedback and participation from API stakeholders.
To that end, please reach out to our team if you would like to attend our bi-weekly sprint reviews to provide feedback and get updates on our efforts.

As a first step for migration, the current API Store domain name, `api.wisc.edu`, will be transferred to be used for the Apigee production API gateway.
This represents a change in usage for this domain beyond a change in underlying vendors.
Using "api" as a third-level domain (e.g. api.example.com) is a common practice for accessing APIs in an organization.
This is separate from the capabilities of discovering, getting access to, and testing APIs from a web browser, which is what api.wisc.edu is being used for with WSO2 API Cloud.

In addition to changing to a new API Management platform, we are using this opportunity to improve our institutional strategy for API Management through the [API Program](https://git.doit.wisc.edu/interop/external-docs/api-program).
Changing the purpose of `api.wisc.edu` is a reflection of that effort and represents a small first step in aligning UW-Madison's API practices towards consistent standards.

## Details

The current API Store domain name, `api.wisc.edu`, will be changing to `wso2apistore.services.wisc.edu` on **September 21st, 2021**.
Please update any bookmarks or internal documentation to reflect this change.

After this change, `api.wisc.edu` will temporarily resolve to a page instructing users to update their bookmarks.
Following that, `api.wisc.edu` will be used for production API calls to Google Apigee.

This change does not affect current API Calls through WSO2 API Cloud's gateway: `gateway.api.wisc.edu`
The eventual migration from `gateway.api.wisc.edu` to `api.wisc.edu` will be communicated in the future with ample time for migration and testing.
However, this change does affect API calls to [WSO2's management APIs](https://cloud.docs.wso2.com/en/latest/management-apis/overview/) that use `api.wisc.edu`.

This change also doesn't affect the API Publisher interface or any domain names for the WSO2 API Cloud test environment.

For any questions or feedback on this change, please email integration-platform@doit.wisc.edu