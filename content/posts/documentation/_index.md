---
title: "Documentation"
summary: "Get documentation for WSO2 API Cloud."
menu:
  intro:
    weight: 10
  navbar:
    weight: 10
---
Our current documentation is for our API Manager vendor: WSO2 API Cloud.

[Start here if you are using API Cloud as a subscriber](../../subscribe-to-apis) (e.g. you are using APIs that others provide).

[Start here if you are using API Cloud as a publisher](../../publish-apis) (e.g. you are publishing APIs for others to use).
