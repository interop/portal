---
title: "API Design Best Practices"
---

Designing an API is an art that should be carefully considered before implementing an API.
By doing more design work upfront, the API will be easier to understand and more flexible for future improvements.
The Interoperability team has put together a list of best practices around API design.
We encourage you to use these practices when designing and creating your APIs.

## Principles

* **Consistency** from API client and publisher point of view.

* **Make APIs, consumer-centric**
    Focus on integrator/developer success and needs rather than strictly following a standard.
    Listen/adopt feedback from Distributed Developer Group.

* **Service Evolution**
    The API should be able to evolve and add new functionality independently from client applications.
    Existing client applications should continue to function without modification.

* These best practices are for APIs created at UW-Madison.

## Existing Standards

If you want to use an existing standard to aid with API design, we recommend using [JSON:API](https://jsonapi.org).
JSON:API takes a more opinionated stance on API design and many languages support it - both from a client and server perspective.
Think of these best practices as a superset of JSON:API.

## Designing Representations (Resources)

Use JSON to represent the resource. JSON is simple, human readable, and popular among developers (as of this writing).
Keep JSON simple and readable.
JSON will be simple and easier to understand if names of your JSON keys are always property names and the JSON objects correspond to entities in data model.

{{< good >}}
```js
{
    "user": [
        {
            "id": "12345", // unique id of the resource for the given API
            "user_id": "{user_id_a}",
            "picture": "{photo_url}",
            "created_time": "2014-07-15T15:11:25+0000"
        }
    ]
}
```
{{< /good >}}

{{< bad >}}
```js
{
    "{user_id_a}": {
        "user": [
            {
                "id": "12345",
                "picture": "{photo_url}",
                "created_time": "2014-07-15T15:11:25+0000"
            }
        ]
    }
}
```
{{< /bad >}}

If multiple formats or media types (also known as Multipurpose Internet Mail Extensions or MIME types) are supported (e.g. JSON, XML, etc.) use the `Accept` request header to determine which media type to return in the response body, and use the `Content-Type` response header when parsing a request body.

Provide support for HTTP `Accept` [header](https://tools.ietf.org/html/rfc7231#section-5.3.2) to support other formats. For example:

{{< good >}}
```
GET https://adventure-works.com/orders/2 HTTP/1.1
Accept: application/json
```
{{< /good >}}

* Have JSON as the default type (i.e. If client doesn’t send Accept HTTP header).
* If the server can not match any of the media types requested by client, it should return HTTP status code 406 (Not Acceptable).
* Use `Content-Type` header in a request (and then in response) to denote the format of the representation.

For example:

{{< good >}}
```
POST https://adventure-works.com/orders HTTP/1.1
Content-Type: application/json; charset=utf-8
Content-Length: 57

{"id":1,"name":"Gizmo","category":"Widgets","price":1.99}
```
{{< /good >}}

If the API doesn’t support the media type denoted by `Content-Type` in request it should return HTTP status 415 (Unsupported Media Type).
Use lowercase words separated by underscores (`_`) to name your keys in JSON ( or elements in XML) in your resources.

See above examples.

* Watch out for APIs that leak the internal implementation details or simply mirror an internal database schema.
The API should model the domain and accomdate the use cases of clients. Avoid doing [a domain model dump](https://blogs.gartner.com/eric-knipp/2013/04/30/api-design/).

* To avoid data corruption due to simultaneous update to a resource  (i.e. two or more `PUT` requests at the same time to the same resource) use [ETag](https://tools.ietf.org/html/rfc7232#section-2.3) and [If-Match](https://tools.ietf.org/html/rfc7232#section-3) headers.
Alternatively use `PATCH` requests for resource updates.

* Use links (Hypermedia as the Engine of Application State or HATEOAS) to represent relationships (links to child entities from parent entity) in the API. Use absolute URLs for links (e.g. https://api.example.com/foo/0001)
This will help API clients avoid extra URI templating or looking through documentation to build URLs.
See external [reference](https://docs.microsoft.com/en-us/azure/architecture/best-practices/api-design#use-hateoas-to-enable-navigation-to-related-resources).

* Avoid [Chatty I/O](https://docs.microsoft.com/en-us/azure/architecture/antipatterns/chatty-io/index) (anti-pattern).

## Design URIs/URLs

* In URLs, nouns (in plural form) are good; verbs are bad.
```
https://adventure-works.com/create-order // avoid
https://adventure-works.com/orders // good
```

* Design relationships in URLs so that it form a [graph](https://en.wikipedia.org/wiki/Graph_theory): `start->traverse->select->traverse`.
The general pattern of this query URL is:
/{relationship-name}[/resource-id]/{relationship-name}.

For example:

{{< good >}}
`/persons/5678/dogs`
{{< /good >}}

* Avoid requiring resource URIs more complex than `/collection/entity/collection`.
Don't go too deep or nested.
Go two levels deep at most.
Too deep or nested collections can be difficult to maintain and is inflexible if the relationships between resources will change in the future.
E.g. `/customers/1/orders/99/products` can split into two URIs `/customers/1/orders` and `/orders/99/products`.
Note that by limiting the number of resources on the URI can increase the load on the server, by making the client make multiple calls to find all data that it requires.
Instead consider how you can denormalize the data and combine related information into bigger resources that can be retrieved with a single request.
This approach needs to balance against the overhead of fetching data that the client doesn’t need.
Also check Chatty I/O anti-pattern above and Extraneous [Fetching anti-pattern](https://docs.microsoft.com/en-us/azure/architecture/antipatterns/extraneous-fetching/index).

* Design URI templates so that they can be easily understand/remember and readable.

* Always use TLS (SSL) everywhere, all the time (no http URLs, should only be https).

## Define operations in terms of HTTP methods and status codes

Conform to HTTP semantics, when in doubt consult the HTTP specifications, [here](https://tools.ietf.org/html/rfc2616) and [here](https://tools.ietf.org/html/rfc7231).

The `GET`, `PUT`, and `DELETE` methods must be idempotent (an operation is idempotent if it can be called multiple times without producing additional side-effects after the first call, i.e. should not modify internal state in the service -- immutable), see [rfc723](https://tools.ietf.org/html/rfc7231#section-4).

### `GET`

Retrieves a representation of the resource specified by the URI.
Response body is the requested resource.

* A successful GET method should return HTTP status 200 (OK).
* If the resource can not be found, the method should return 404 (Not Found).

### `POST`

Creates a new resource at the specified URI.
The body of the request provides the details of the new resource.
Note that POST can also be used to trigger operations that don't actually create resources.

* A successful POST method should return HTTP status 201 (Created).
* The URI of the new resource should be included in the Location header in the response.

The response body contains a representation of the resource.

* If the method does some processing but doesn’t create a new resource, the method should return HTTP status 200 and include the result of the operation in the response body.
* If there is no result to return, the method can return HTTP status 204 (No Content) with no response body.
* If the client sends invalid data in the request, send HTTP status 400 (Bad Request).

Include additional information about the error in the response (also see the section below on the format of error response).
HTTP methods can have asynchronous semantics, where the method returns a response immediately, but the service carries out the operation asynchronously.
Use HTTP 202 (Accepted - https://tools.ietf.org/html/rfc7231#section-6.3.3) to indicate the request was accepted for processing, but the processing is not yet completed.

### `PUT`

Either creates or replaces the resources to be created or updated.
The body of the request message specified the resource to be created or updated.

* If PUT method creates a new resource, it should return 201 (Created).
* If PUT method updates an existing resource, it should return either 200 (OK) or 204 (No Content).
* If it’s not possible to update the resource, return HTTP status 409 (Conflict).
* If PUT method is not supported it should return 501 (Not Implemented).

### `PATCH`

Perform a partial update to the resource from the request body (change set).

* The specification for PATCH [method](https://tools.ietf.org/html/rfc5789) doesn’t define a format for patch documents, so the API should inferred the format from the media type in the request.
* If the patch document format isn’t supported send a HTTP status 415 (Unsupported Media Type).
* If the patch document is malformed send a HTTP status 400 (Bad Request).
* If the patch document is valid but the changes can’t be applied to the resource in its current state sends a HTTP status 409 (Conflict).
* If PATCH method is not supported it should return 501 (Not Implemented).
* If JSON is used as the type use [rfc7396](https://tools.ietf.org/html/rfc7396) for JSON merge patch.

If original resource can contain explicit null values, use JSON patch, defined in [rfc6902](https://tools.ietf.org/html/rfc6902).
Document these facts in your API documentation for clients.
If XML is used as the type use [rfc5261](https://tools.ietf.org/html/rfc5261) for patching.
Document these in your API documentation for clients.

### `DELETE`

Removes the resource at the specified URI.

* If successful should respond with HTTP status 204 (No Content).

No further information in the body.

* If the resource doesn’t exist send HTTP 404 (Not Found).
* If the DELETE method is not supported it should return 501 (Not Implemented)

### Status codes

Status Code | Usage
:--------|:------
`200 OK` |   The request completed successfully.
`201 Created` |  A new resource has been created successfully. The resource’s URI is available from the response’s Location header (see GET method above).
`202 Accepted` | Request accepted for processing, but processing is not yet completed.
`204 No Content` | An update to an existing resource has been applied successfully.
`400 Bad Request` | The request was malformed. The response body will include an error providing further information.
`401 Unauthorized` | Login/Access attempt with invalid credentials/token.
`403 Forbidden` | Don’t have permission to perform the  requested operation (generally related to role based access control).
`404 Not Found` | The requested resource didn’t exist.
`405 Method Not Allowed` | The method is known by the server, but not supported by the target server. The server MUST generate an `Allow` header field in a 405 response containing a list of the target resource's currently supported methods.
`500 Internal Server Error` | An uncaught/unrecoverable error or  a bug in the server code.
`501 Not Implemented` | If the server does not recognize the request method and is incapable of supporting it for any resource.
`503 Service Unavailable` | This status is returned when too many requests are going to the same controller.

For example:

Resource| POST |  GET | PUT | DELETE
--------|------|------|------|------
`/customers` | Create a new customer|Retrieve all customers|Bulk update of customers| Remove all customers
`/customers/1` | See notes under POST method above|Retrieve the details of customer 1|Update the details of customer 1 if it exists|Remove customer 1
`/customer/1/orders` | Create a new order for customer 1|Retrieve all orders for customer 1|Bulk update of orders for customer 1| Remove all orders for customer 1
`/customer/1/orders` | Create a new order for customer 1|Retrieve all orders for customer 1|Bulk update of orders for customer 1|Remove all orders for customer 1

## Error Responses

Make your API provide detailed and helpful error messages.
Running into errors is how your API clients will learn how to use your API.
Avoid generic error messages such as "Bad Request".
Instead, return a helpful error message that will allow API clients to know how to revise their request in order to make a successful API call.

For example, consider a "dogs" API with a resource to create a new dog. API clients must include the color in the request body.

If an API client did not include the color in their request, the API should respond with a `400` (Bad Request) error code with a message in the response body: "Color is a required field."

Use the following format when defining error responses in your APIs.
`error_code` can be API level business errors.
`more_info` can provide a way to get more information about the error.
If you provide optional error_code, be sure to provide additional details in `more_info`.

```json
{
    "status": "HTTP status code (integer)",
    "developer_message": "Verbose, plain language description of the problem for the application developer with hints about how to fix it.",
    "user_message":"Pass this message onto the app user if needed(optional)",
    "error_code": "12345(optional)",
    "more_info": "https://dev.teachdogrest.com/errors/12345(optional)"
}
```

## Filter and Paginate Data

Before adding pagination consider the API at hand.
Does it need pagination or does it make sense to not to paginate.
For example, consider an API just has 50 resources in the collection, or your API needs consistent reads to get all resources in a collection.
On the other hand not having pagination for an API that produces large collection sizes might have negative effects (e.g. APIs utilized by mobile apps). If you decide to have pagination, have a default collection size of 25.
25 is used as a default in some of the popular [APIs](https://developer.github.com/v3/#pagination) when responding to a GET collection request.

Use query parameters `offset` (resource index starts at 1) and `limit` to filter out/paginate  resources when client request a collection from a GET request.
Providing these parameters especially important for mobile client applications (to limit the amount of data returned by a request) and not to waste network bandwidth.

Use readable, short query string parameters for filtering collections.

{{< good >}}
`/dogs?color=brown&state=running&location=park`

`/persons/5678/dogs?color=brown`
{{< /good >}}

If filter operations return no results provide an empty collection with 200 OK. For example:

request | example
:--------|:------
`/orders` | Send first 25 (default) orders
`/orders?offset=10` | Send records 11 to 25
`/orders?limit=10` | Send first 10 orders
`/orders?limit=25&offset=50` | Send records 51 to 76
`/dogs?color=brown&state=running&location=park` | Find all brown dogs running in the park (filter example)


## API Versioning

Do not version APIs.
Continue to maintain backward compatibility in your APIs (i.e. a client written against one version of an API will continue to work the same, without modification against a future version of the API).
This will require maintaining additional code for the API implementation (e.g. using [adapter pattern](https://en.wikipedia.org/wiki/Adapter_pattern).
If it is necessary to change a representation in a way that is not backward compatible, a new resource should be created using new representation, and old resources can be maintained according to a depreciation policy.
See [this ebook](https://cloud.google.com/files/apigee/apigee-web-api-design-the-missing-link-ebook.pdf) for additional reading about API versioning.

## Data Types, Tooling, Documentation and Collaboration
* Use [OpenAPI 3.0.2](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md) specification for API definitions and data types in your APIs.
Date/Datetime is encoded in JSON string using [ISO 8601](https://xml2rfc.tools.ietf.org/public/rfc/html/rfc3339.html#anchor14).
See an example [here](https://docs.microsoft.com/en-us/openspecs/ie_standards/ms-es3ex/e13c8110-e94f-49ef-9316-0bef4dd05833
).

* Consider using [Swagger Hub](https://swagger.io/tools/swaggerhub/) for collaboration with business domains/partners.

## References

* https://cloud.google.com/files/apigee/apigee-web-api-design-the-missing-link-ebook.pdf
* https://docs.microsoft.com/en-us/azure/architecture/best-practices/api-design
* https://docs.microsoft.com/en-us/azure/architecture/antipatterns/chatty-io/index
* https://docs.microsoft.com/en-us/azure/architecture/antipatterns/extraneous-fetching/index
* https://developer.github.com/v3/
* https://developer.twitter.com/en/docs
