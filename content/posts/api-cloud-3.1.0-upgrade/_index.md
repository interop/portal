---
title: "API Cloud Upgrade to 3.1.0"
---

## Update

2020-08-03

WSO2 encountered issues with the upgrade and had to perform a rollback. We do not have an updated date and time for when WSO2 will attempt again.

## Summary

- API Cloud is upgrading to version 3.1.0 of API Manager on July 31, 2020.
- During the upgrade, the API Store and API Publisher web interfaces will be unavailable.
- **API traffic will be unaffected before, during, and after the upgrade. No action is required from API publishers and subscribers.**

## Introduction

Our vendor-hosted API management platform, [WSO2 API Cloud](https://wso2.com/api-management/cloud/), will be going through a major version upgrade on July 31,2020.
API Cloud is a software-as-a-service (SaaS) implementation of the open source platform, WSO2 API Manager.

This upgrade will bring our version of API Manager from 2.6.0 to 3.1.0.
In addition to a new look and feel of the web interfaces for API publishers and API subscribers, version 3 of API Manager offers some new features.

![New portal web interface](./images/new-portal.png)

Take a look at this [post from WSO2 for more information about what's new in version 3 of API Manager](https://wso2.com/library/articles/2019/10/whats-new-in-wso2-api-manager-version-3.0/).

## Upgrade Process

This upgrade is scheduled for a maintenance window from July 31, 2020, at 11:00 p.m. to August 1, 2020, at 7:00 p.m.
During this time, the API Store and API Publisher web interfaces will be unavailable.

Additionally, API Manager administrative APIs will only respond to `GET` requests during the maintenance window.
Administrative APIs are used to interact with API Manager itself, and are unrelated to published APIs that are proxied through API Manager.

**This upgrade does not affect the availability of existing APIs. API traffic will continue to operate before, during, and after the upgrade.**
Access tokens, consumer keys, and consumer keys will also be unaffected by this upgrade.

## Next Steps

The DoIT Integration Platform team is testing the new version of API Manager prior to the upgrade
and will test that the upgrade is successful after WSO2 is done upgrading our environment.

If you have any questions, feel free to [contact us](mailto:integration-platform@doit.wisc.edu).
