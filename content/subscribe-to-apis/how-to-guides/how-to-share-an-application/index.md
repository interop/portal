---
title: "How to Share an Application"
date: 2021-01-22T12:00:00-06:00
draft: false
---

An application in the API Store is owned by a single person, but it can be shared with a group.
Follow these instructions if you would like to share your application with a group:

1. [Create a group in Manifest using these instructions](https://kb.wisc.edu/iam/page.php?id=25878).
2. Grant Read and View privilieges to your group for our team's group (`uw:org:ais:ais-admins`), so that our team can add your group to our authorization process.
3. Send an email to integration-platform@doit.wisc.edu containing the full name of the Manifest group (e.g. `uw:my:cool:Group`).
4. We will map the Manifest group to a group in the API Store. This will allow members of the Manifest group to login and automatically be added to the API Store group. We will respond to you with the name of the WSO2 Group (e.g. Internal/MyCoolGroup).
5. [Follow these instructions to add the group name to the application](https://cloud.docs.wso2.com/en/latest/learn/consume-apis/share-an-application-with-your-team/) that you would like to share.

