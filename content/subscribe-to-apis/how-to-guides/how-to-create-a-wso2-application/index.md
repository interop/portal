---
title: "How to Create a WSO2 Application"
date: 2019-11-25T11:46:22-06:00
draft: false
---
Every authenticated user in the WSO2 API Store is given an Application called ‘DefaultApplication’ when they are provisioned in the system,
but you will want to create additional WSO2 Applications that tie one or more APIs together for use by your application.

Since a WSO2 Application is a logical extension of the application you are building,
it is best to give meaningful names to these Applications.
For example, if you are building an application that displays timetable information for a department, you might aggregate
three APIs--such as a Terms API, Subject API, and a Course API--under the WSO2 Application named ‘departmental-timetable’.

### Create a new Application
1. Log in and click APPLICATIONS in the main menu
2. Click ADD APPLICATION on the blue bar
    ![Add application](images/add_application.png)
3. Fill in the form
    1. Enter a meaningful name for the WSO2 Application based on your application that will be using it.
    2. Enter a group name (optional; usually leave this blank). If you belong to a group and want to share this Application and its subscriptions with other members of the group, enter the name of the group. See [this documentation](https://docs.wso2.com/display/APICloud/Sharing+Applications+Between+Multiple+Groups) for more details.
    3. Leave the Per Token Quota field value at ‘Unlimited’.
    4. Optionally provide a description for your Application.
    ![Application form](images/application_form.png "Application form")
