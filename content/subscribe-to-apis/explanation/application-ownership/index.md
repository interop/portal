---
title: "Application Ownership"
date: 2021-01-22T12:00:00-06:00
draft: false
---

In WSO2 API Cloud, an account is associated with one-to-many applications that you create.
An application contains one-to-many subscriptions to APIs.
Applications are meant to separate access to APIs by use-case.

For example, an application could be called "Department Email List" and contain a subscription to the Google Groups API.

An application contains up to two sets of OAuth key pairs, for sandbox and production environments.
[For more information on OAuth, see this document](../oauth).
The OAuth keys act as a service account, since they are meant for running software/processes as an API Cloud application rather than as a user.

Although API calls are authenticated using OAuth keys, the application is created and owned by an individual user.
Unfortunately, API Cloud does not have a good method to have an application owned by a non-person, such as a group. It does allow an application to be shared with a group, while still being owned by a primary person.
[If you would like to learn more about how to share an application with a group, see this how-to guide](../../how-to-guides/how-to-share-an-application).

Application ownership can also be transferred to another user.
If you would like to transfer application ownership to somebody else on your team, please send an email to integration-platform@doit.wisc.edu and include the following information:

- The name of the application.
- Your NetID, as the current owner of the application.
- The NetID of the person who should be the new owner of the application.

To align with the recommendations of DoIT CyberSecurity, we do not grant access to the API Store for non-person accounts (i.e. service accounts).
