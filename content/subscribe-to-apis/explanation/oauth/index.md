---
title: "OAuth2"
date: 2019-12-03T16:46:22-06:00
draft: false
---

WSO2 API Cloud uses OAuth2 for authorizing API calls. This article requires a basic understanding of OAuth2.
For more information on OAuth2, [see this article](https://blogvaronis2.wpengine.com/what-is-oauth/).

In summary, OAuth2 involves exchanging information for access tokens which are used to authorize API calls. 
Access tokens are valid for a finite period of time and they should be treated like passwords.
Do not share access tokens.

The information in this article references configurations set in the application information in the API Store.
To follow along, go to the [API Store](https://wso2apistore.services.wisc.edu), click "Applications", and click on an application (e.g. Default Application).
Then, click on either `Sandbox Keys` or `Production Keys`.

## Keys

![Key](./images/keys.png)

OAuth2 uses two keys for requesting access tokens: The Client ID and Client Secret.
These keys are also known as the Consumer Key, and Consumer Secret, respectively, and these two names are interchangeable for the purposes of this document and what is seen in WSO2.
This document will use Client ID and Client Secret, since those are the official names defined in the [OAuth2 specification](https://tools.ietf.org/html/rfc6749#section-2.3.1).

Most APIs in WSO2 have a sandbox environment and a production environment.
Each environment in your application has unique keys.
WSO2 will determine which environment to forward your API request to based on which keys were used to generate the access token sent in the request.

**The Client Secret should be treated like a password and kept secret.
The Client Secret should also not be used in any token requests made by the browser.**

## Grant Types

![Grant types in the API Store](./images/grant-types.png)

You can configure which grant types are supported by your application in the [API Store](https://wso2apistore.services.wisc.edu).
The grant types you should use depend on the kind of application you're using, and the type of data exposed by the API you're using.
Below are some recommendations for the grant types we recommend using, when to use them, and the grant types we don't recommend.

For more information on using these grant types, we recommend using [WSO2's documentation for their Token API](https://docs.wso2.com/display/APICloud/Token+API).
Their documentation will show examples using `https://gateway.api.cloud.wso2.com`.
Please use our domain instead: `https://gateway.api.wisc.edu`.

All OAuth2 tokens issued by WSO2 have a validity period of 3600 seconds (1 hour).
The only exception is the client credentials grant, where the token validity can be adjusted by the API subscriber.

### Client Credentials

Use this the client credentials grant type for making server-side requests to APIs that don't require knowing who the end-user of your application is.
For example, APIs that expose non-user specific data (e.g. course information, building locations) or APIs meant to expose a set of bulk information.

WSO2 allows API subscribers to set the validity period of client credentials tokens in the application information in the API Store.

*Note: We recommend making the validity period of tokens short: 3600 seconds (1 hour) or less.
Using a longer validity period increases the risk for data breaches if the access token is stolen or compromised.*

For more information on the Client Credentials grant type, [see this article](https://www.oauth.com/oauth2-servers/access-tokens/client-credentials/).

### Authorization Code and Implicit

Both the Authorization Code and Implicit grant types are used when the API needs to identify the end user of an application.
For example, if the API exposes data about a specific person, these grant types are used to get an access token that can only access a specific person's data through the API.

These two grants require directing users to `https://gateway.api.wisc.edu/authorize` which uses NetID Login to authenticate the end user. After authenticating, WSO2 will redirect back your application using the "Callback URL" registered in the application information in the API Store.

#### Authorization Code

This grant type is abbreviated as "Code" in the API Store.
Use the authorization code grant type when your application has a server-side component that can make requests to get access tokens for identifying the end user of application.

The Authorization Code grant type requires making two requests: The first request is to get an authorization code, and the second request exchanges the authorization code for an access token.
The request to get an access token requires sending a `POST` request with the `client_secret`, so this request should not be made from the browser.

For more information on the Authorization Code grant type, [see this article](https://developer.okta.com/blog/2018/04/10/oauth-authorization-code-grant-type).

#### Implicit (Do Not Use)

The implicit grant type is similar to Authorization Code, except it only requires one request to get an access token.
Implicit is mostly commonly used for applications that don't have a server delivering dynamic content (e.g. single page applications), but it's being phased out in favor of using the Authorization Code grant type with the [OAuth2 PKCE extension](https://oauth.net/2/pkce/).
WSO2 API Cloud does not currently support the PKCE extension.

We don't recommend using the Implicit grant type, favoring the Authorization Code grant type instead if you have a server that can make the requests to get access tokens.

For more information on the Implicit grant type and why it's no longer recommended, [see this article](https://developer.okta.com/blog/2019/05/01/is-the-oauth-implicit-flow-dead).

### Refresh Token

When requesting an access token with the Authorization Code grant type, the response will also contain a refresh token.
The refresh token can be used in the `refresh_token` grant type to exchange the refresh token for a new access token.
The benefit is that you can get a new access token without making the user go through the authorization process again.

For more information on the Refresh Token grant type, see [WSO2's documentation](https://docs.wso2.com/display/APICloud/Refresh+Token+Grant).

### SAML2 (Do Not Use)

This grant type involves exchanging SAML assertions for an access token that identifies the end user.
This grant type is not set up in our instance of WSO2 API Cloud and we don't recommend using it.
For more information on this grant type, [see this article](https://tools.ietf.org/id/draft-ietf-oauth-saml2-bearer-16.html).

### IWA-NTLM (Do Not Use)

This is not configured in our instance of WSO2 API Cloud and we don't recommend using it. For more information about this grant type, [see this article](https://medium.com/@farasath/integrated-windows-authentication-with-kerberos-and-wso2-identity-server-ffcd8263a0f1).

### JWT (Do Not Use)

This grant type allows someone to exchange a JSON Web Token (JWT) for an access token.
This grant type is not configured in our instance of WSO2 API Cloud.

For more information on this grant type, [see the specification](https://tools.ietf.org/html/rfc7523).
Please contact us if you require this grant type for your application: integration-platform@doit.wisc.edu

### Password (Do Not Use)

This grant type involves a user submitting their username and password to get an access token.
The Password grant type is no longer recommended, and our instance of WSO2 API Cloud is not configured to support it.

For more information on the Password grant type, [see this article](https://oauth.net/2/grant-types/password/).

## Scopes

In OAuth2, APIs use scopes to permit access to resources and methods (e.g. POST, GET, etc.).
For example, a common pattern is to have separate scopes for `READ` and `READ/WRITE`, to allow different levels of access to an API depending on the client application.

APIs in the API Store use scopes to authorize client applications who have gone through the necessary approvals to get access.
Getting access to an API depends on the instructions documented by the publisher of the API and the data classification(s) used.
Some APIs that use public data won't need scopes, so approval to use the API will be automatic.

At the bottom of the page for your application keys (Sandbox or Production), there is a dropdown menu that contains all the scopes available for the APIs your application is subscribed to.
A scope listed in the dropdown menu does not necessarily mean you have access to use that scope.


### Example

In this example, the API store shows the application is subscribed to the World Bank API which has the scope `World_Bank_Read`.
You can select this in the API Store and generate a new `client_credentials` grant type token.

![World bank read scope](./images/scopes.png)

This is equivalent to making this request with `curl`:

```sh
curl -k -d "grant_type=client_credentials&scope=World_Bank_Read" \
-u '<your_client_id>:<your_client_secret>' \
https://gateway.api.wisc.edu/token
```
