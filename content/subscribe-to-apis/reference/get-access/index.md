---
title: "Get Access to the Store"
date: 2019-11-22T13:50:00-06:00
draft: false
---

To get access to the API store, send an email to integration-platform@doit.wisc.edu and include the following:

* Your NetID.
* The APIs you're wanting to use.
Access won't be automatically granted to APIs in the API store, but this will help give us a better idea of your use case.
