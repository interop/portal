---
title: "Subscribe to an API"
date: 2019-11-22T08:47:11-06:00
draft: false
weight: 2
---

## Browse and Subscribe

In this short tutorial you will subscribe to the Terms API by adding a subscription to your DefaultApplication.

Navigate to the Terms API and open the API-details page. In the Applications panel, if ‘DefaultApplication’ is not selected, please select it.


> *If you have been working with in the API Store at [https:/api.wisc.edu](https://wso2apistore.services.wisc.edu) for some time before coming to this tutorial, you may have deleted your DefaultApplication.
In this case, select an existing Application from the drop-down or see [How to Create a WSO2 Application](../../how-to-guides/how-to-create-a-wso2-application) to learn how you create a new Application.*

Notice that in the Tiers drop-down there is just the Bronze tier for the Terms API.
The subscription tiers are set up by the API publisher and control how many requests per minute your Application can send to the API.
The possible tier levels are Unlimited, Gold (5000 requests per minute), Silver (2000 requests per minute)  and Bronze (1000 requests per minute).
![Terms subscription](images/terms_subscribe.png)

Click ‘Subscribe’ and a confirmation dialog should open.
![Subscription successful](images/subscription_success.png)

Click ‘View Subscriptions’ on the pop-up.
The API Store takes you to the Application details page which shows the newly-subscribed Terms API on the Subscriptions tab.
You should see the subscription tier that was chosen when you subscribed; Bronze, in this case.
![Application details with Terms subscribed to](images/application_details_terms_subscribed.png)

## Create Keys to Use the API
On the DefaultApplication details page, notice the ‘Production Keys’ and ‘Sandbox Keys’ tabs.
Each API available through the API Store can have a Sandbox endpoint and a Production endpoint.
These endpoints generally correspond to test and production deployments of the API implementation.

> *The API Publisher determines which endpoints are available.
> So although you can create keys in your DefaultApplication for both Production and Sandbox endpoints,
> if the API you are calling does not support--for example--the Sandbox endpoint,
> and you use the Sandbox token to call the API, you will get a 403 (Forbidden) error.*

When a WSO2 Application is first created--or in the case of the DefaultApplication which is created automatically--there are no keys associated with the Application,
so the first thing to do is click the ‘Generate keys’ button on both the Production and Sandbox tabs.

Keep the ‘Grant Types’ checked as they are, leave the ‘Callback URL’ blank, keep the ‘Access token validity period’ at 3600 seconds (1 hour).

![generate keys](images/generate_keys.png)

After clicking ‘Generate keys’, the screen refreshes with Consumer Key/Secret populated and a new Access Token that can be used to test the Terms API.
![keys generated](images/keys_generated.png)

To view the keys and access token, click ‘Show keys’.
Whenever the ‘Regenerate’ button is clicked, a new access token is created.
The ‘Update’ button is used to change the callback URL, but since we are not using that value, you can ignore it.

The Consumer Key and Consumer Secret are created once for the Production endpoint and once for the Sandbox endpoint;
those keys are then used to create new access tokens as often as they are needed.
You will use these keys in your own application to get a token and then use the token to make calls to APIs.

Now that you have a WSO2 Application with keys and an access token for both Production and Sandbox, you can try out the API.
