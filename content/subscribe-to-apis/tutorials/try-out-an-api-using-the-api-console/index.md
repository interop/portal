---
title: "Try out an API using the API Console"
date: 2019-11-25T13:34:47-06:00
draft: false
weight: 3
---

In this tutorial you will use the API Console of the Terms API to test out calls that can be made to the API.
Assuming you have successfully completed the [Create Keys to use API](../subscribe-to-an-api#create-keys-to-use-the-api) tutorial, browse to the Terms API and select the API Console tab.

In the API Console tab you can see all the term resources that can be requested.
For the Terms API these are all GET requests, but they can also be POST, PUT, etc.
![Terms API Console](images/terms_api_console.png "Terms API Console")

Let’s call the `/terms/now` resource, which will return data on the current term.
Click on the `/terms/now` resource blue box to expand it and see the details about this GET request.
![terms/now GET request](images/terms_now_call.png "terms/now GET request")

As you can see, the request takes no parameters.
The response content type is `application/json` and a sample response is shown for a successful request (a 200 response code).

Click ‘Try it out’ to make a call to the API.
Since the request takes no parameters, just click the execute button to make the call.
![/terms/now success response](images/terms_now_success.png "/terms/now success response")

When the response comes back the display contains several pieces of information.
A full [curl](https://curl.haxx.se/docs/manpage.html) command of the request is displayed in the first black box.
The request URL that was used for the request is also displayed and as you can see that request was sent to `gateway.api.wisc.edu`.
These two pieces of information will be helpful as you develop your application and make calls to an API through WSO2 API Cloud.

If the call was successful, you received a 200 response.
The response body is displayed next followed by any response headers returned.

Instead of a 200 response, you may get another response code, such as a 401 (Unauthorized).
![/terms/now unsuccessful response](images/terms_now_unsuccessful.png "/terms/now unsuccessful response response")

This response is very likely if your access token expired.
To regenerate an access token, head back to the WSO2 Application that the Terms API is subscribed to; in this case the DefaultApplication.
You can see which Application is using the API from the information at the top of the API Console tab:
![Application using API](images/application_using_api.png "Application using API")

You can also see whether Production or Sandbox keys are being used and what the current access token is; the string following ‘Authorization: Bearer’, also known as the bearer token.

After navigating back to the DefaultApplication (APPLICATIONS | click ‘View’ on the row with DefaultApplication) and regenerating the token for Production, head back to the Terms API and try the /terms/now resource call once again.
