---
title: "First time at the API Store"
date: 2019-11-22T08:46:11+01:00
draft: false
weight: 1
---

Go to: [https://wso2apistore.services.wisc.edu](https://wso2apistore.services.wisc.edu).
There you will see all APIs represented as tiles.
Since you aren’t logged in yet, you can only search and browse publicly available APIs.


## Log in
Log in by clicking the ‘Sign In’ button (the icon looks like ![sign-in](images/sign-in.png "sign-in")).
If you haven’t already authenticated using your NetID and DUO, you will be prompted to do so, after which you will be routed back to the API Store as an authenticated user.

> *If you see the following screen, please [follow these instructions to request access](../../reference/get-access).*
>
> ![log in not permitted](images/cant-log-in.png "log in not permitted")

Once you have successfully signed in to  the API Store, you will see your _netid@wisc.edu_ in the upper right-hand corner of the screen.

## Explore an API

As an example API, let’s explore the Terms API.

Look for--or search for--the Terms API. The tile for the Terms API has a big ‘T’ on it and it says ‘Terms-v1’. Note there are multiple pages of API tiles, so when you browse, the Terms API may not be shown on the first page.

The search area is located just below the black bar.

![terms tile](images/terms-tile.png "terms tile")

Once you see the Term API tile, click it.

The API-details page shows general information about the API and has tabs that explore more features about the API including an _API console_ to try out the API
(see the [Try out an API using the API Console](../try-out-an-api-using-the-api-console) using the API Console tutorial for more on this).

Check out additional developer-supplied documentation under the _Documentation_ tab.
Note that not all APIs will have additional documentation since that decision is left up to the API publisher.

![developer documentation/subscribe](images/dev-docs-subscribe.png "developer documentation/subscribe")

You’ll notice in the upper-right hand corner there is Subscribe panel with an Applications field and a button to subscribe.
We’ll get back to subscribing to an API in another tutorial, but for now just know that it is through an Application that you subscribe to APIs.

## Explore a WSO2 Application

A WSO2 Application is the logical representation of an external application that uses APIs such as a mobile app, web app, or backend service, i.e., the applications developers build.

The first time you click on APPLICATIONS on the main menu in the upper left, you will see one pre-defined WSO2 Application called DefaultApplication:

![DefaultApplication](images/default-application.png "DefaultApplication")

WSO2 Applications contain subscriptions which is how you work with an API.

Click on the ‘DefaultApplication’ link or on ‘View’ to see the details of the DefaultApplication.
The detail view has tabs for Production/Sandbox Keys and for Subscriptions.
The Subscription tab should list no subscriptions at this point, since none have been added to the application.

![Default Application](images/default_application_initial.png)
