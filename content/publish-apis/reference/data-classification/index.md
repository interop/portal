---
title: "Data Classification"
date: 2019-11-22T13:39:00-06:00
draft: false
---

WSO2 API Cloud proxies API traffic and temporarily stores data if caching is enabled.
API Cloud has been approved to proxy and cache API traffic for all four data types: restricted, sensitive, internal, and public.
For more information on data classifications in API Cloud, [see this how-to guide](../../how-to-guides/security-approval).
