---
title: "Status"
date: 2019-11-22T14:07:00-06:00
draft: false
---

WSO2 provides a status page for all services that make up API Cloud: http://uptime.cloud.wso2.com

UW-Madison is using the gateway listed as “WSO2 API Cloud Gateway”: http://uptime.cloud.wso2.com/1901402
