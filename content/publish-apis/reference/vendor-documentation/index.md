---
title: "Vendor Documentation"
date: 2019-11-22T13:57:00-06:00
draft: false
---

The vendor (WSO2) provides [documentation for the entire API Cloud product](https://docs.wso2.com/display/APICloud/WSO2+API+Cloud+Documentation).
Some guides provided by the vendor will instruct users to contact their support to turn on certain features for individual APIs.
The Interoperability team requests that you instead initiate these kind of requests through us so that we can serve as the point of contact for the vendor.
Please send an email to integration-platform@doit.wisc.edu if you encounter something in WSO2’s documentation that requires contacting their support.
