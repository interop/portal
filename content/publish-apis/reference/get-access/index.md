---
title: "Get Access to the Publisher"
date: 2019-11-22T13:50:00-06:00
draft: false
---

To get access to publish APIs, send an email to integration-platform@doit.wisc.edu and include the following:

* Your NetID.
* Your use case for using the API publisher.
You could be publishing an API, or just wanting to try out the API publisher.
* The department or team that you’re representing.

Once we enable access, we will provide links to two Manifest groups that you can use to control who has access to use and edit your API.
[You can follow this how-to guide](../../how-to-guides/restricting-edit-access) to control who has access to edit your API.
For more information on restricting subscriber access to APIs, [follow this how-to guide](../../how-to-guides/restricting-subscriber-access).

As part of enabling access to the publisher, we will also give you access to the store so that you can test and use your API after publishing.
For more information on using the API store, please [see this article](../../../subscribe-to-apis).
