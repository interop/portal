---
title: "Deployment Region"
date: 2019-11-22T13:39:00-06:00
draft: false
---

WSO2 API Cloud uses AWS as its infrastructure provider. The publisher, API Store, and gateway are deployed in the [AWS North Virginia (us-east-1) region](https://aws.amazon.com/about-aws/global-infrastructure/regions_az/).
If you are also using AWS for your API, consider using us-east-1 to minimize the latency of proxying traffic through API Cloud.

For Azure, the closest region geographically is East US.
For Google Cloud Platform, it’s N. Virgina (us-east4).
