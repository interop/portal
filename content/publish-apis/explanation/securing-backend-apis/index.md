---
title: "Securing Backend APIs"
date: 2019-11-22T13:39:00-06:00
draft: false
weight: 4
---

WSO2 API Cloud uses OAuth2 to authorize API calls between client applications and the API gateway.
API calls proxied between the API gateway and the backend API can be authenticated is several ways.
You should use at least one of these methods to secure your backend API, even if your API is only exposing public data.
We recommend using multiple methods in concert.

## Basic Authentication

Basic auth is the simplest way to authenticate calls to a backend API, which involves sending a username and password in each request.
To get started using basic auth, [see this how-to guide](../../how-to-guides/basic-digest-auth).

## Digest Authentication

Digest authentication is similar to basic authentication, but is more secure, and prevents replay attacks.
It applies an MD5 cryptographic hash using nonce values (a one-time-use string) to the credentials before sending them to the backend API.
To get started using digest authentication, [see this how-to guide](../../how-to-guides/basic-digest-auth).

## JSON Web Token (JWT)

TODO: This section still needs to be filled in as part of [IOPB-54](https://jira.doit.wisc.edu/jira/browse/IOPB-54).

## Mutual TLS (Mutual SSL)

Mutual TLS involves exchanging public certificates between your backend API and API Cloud to ensure that API calls are authentically coming from the API gateway.
For more information on setting up mutual TLS in API Cloud, [see this how-to guide](../../how-to-guides/enable-mutual-tls).

## Allowing API Gateway IP Addresses

If you are able to put your backend API behind a firewall, you can allow traffic from only the API gateway.
For more information on this method, [see this reference](../../reference/ip-addresses).
