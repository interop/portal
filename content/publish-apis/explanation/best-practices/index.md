---
title: "Best Practices for Publishing APIs"
date: 2019-11-22T13:30:00-06:00
draft: false
weight: 3
---

## API Design

API Cloud helps provide tooling to design and document APIs, but this still leaves a lot of flexibility in the implementation of resources, response bodies, query parameters, and other key API design traits.
The Interoperability team put together some best practices around API design and we encourage you to use them for your APIs: [“University of Wisconsin REST API Design Best Practices”](../../../posts/api-design-best-practices.md)

## Restrict Publisher Access Control

WSO2 API Publisher is a shared environment where departments across the university can publish their APIs.
We **highly recommend** restricting access to edit your APIs to just people in the Manifest group that was sent when you got access to the publisher.
For more information about how to restrict publisher access control for your APIs, [follow this how-to guide](../../how-to-guides/restricting-edit-access).

If the option "All" is chosen for "Access Control" when publishing an API, everyone who has access to the API Publisher will be able to edit and delete your API - please avoid doing this.

{{< good >}}
![Good access control](./images/good-access-control.png)
{{< /good >}}

{{< bad >}}
![Bad access control](./images/bad-access-control.png)
{{< /bad >}}

## Use HTTPS

All traffic between a client application and API Cloud uses HTTPS, supporting TLS 1.0, 1.1, and 1.2.
We recommend that your backend API uses HTTPS, even for public data, to ensure that all traffic between API Cloud and the backend API is secure.
If possible, make sure your backend API only supports TLS 1.2 or greater.

## Limit Mediation in the API Manager

API Cloud, like most API Managers, can help protect backend APIs by enabling lightweight features like quotas, throttling, and caching.
API Cloud also has the ability to mediate API requests and responses to, for example, convert between XML and JSON, add request/response headers, and change query parameters.
While this seems attractive to have this kind of mediation in a central location, it becomes cumbersome to maintain distributed business logic and increases lock-in with the vendor.
We recommend handling any mediation in your backend API to better follow the principal of [smart endpoints with dumb pipes](https://medium.com/@nathankpeck/microservice-principles-smart-endpoints-and-dumb-pipes-5691d410700f).

For more information on message mediation in API Cloud, please see [the vendor documentation](https://docs.wso2.com/display/APICloud/Message+Mediation+for+WSO2+API+Cloud), but know that we recommend avoiding this feature if possible.
If you are unsure whether you should use message mediation for your API, feel free to send us an email and we can discuss your specific use case: integration-platform@doit.wisc.edu

## Detailed and Visible Documentation

Documenting your API through OpenAPI/swagger and describing the steps for access and approval will help subscribers discover and learn how to use your API.
Your goal should be that any API subscriber can implement an integration with your API without any “out-of-band” information or context, besides any necessary access and approval steps.
If you find yourself repeatedly sharing the same information about your API through email or other communication, it is usually an indicator that your API documentation is lacking important information.
Documentation can also reside in other websites (e.g. KB) but we recommend providing the URL for external documents in the description of your API in API Cloud.

Documentation should also be highly visible to allow your API to be discoverable.
When publishing an API, documentation can be made public during the “Design” step.
Making documentation public does not mean that the API itself is public.
To manage access to API resources, [follow this how-to guide](../../how-to-guides/restricting-subscriber-access).
We recommend making documentation public as a default. Make sure that example data, like request and response bodies, use mock or fake data.
