---
title: "API Cloud Components"
date: 2019-11-20T16:45:00-06:00
draft: false
weight: 2
---

API Cloud is made up of three main components: the publisher, API store (Developer Portal in the diagram below), and API Gateway.

![Components](./images/components.png)

## Publisher

https://publisher.api.wisc.edu

The API publisher is a tool to allow someone to publish and manage information about the APIs they provide.
The publisher doesn’t provide the ability to implement APIs (e.g. connecting directly to a database).
Instead, the publisher is meant to help design, manage, and monitor APIs that are deployed outside of API Cloud.
API publishers can design APIs using tooling provided in the publisher interface, or upload a OpenAPI specification to start publishing an API.
For more information on OpenAPI, [see this article](https://swagger.io/docs/specification/about/).

After designing an API, API publishers can configure API Cloud to proxy traffic to their backend API.
Traffic throttling and quota tiers can be configured to protect APIs from excessive or sudden traffic. API Cloud also supports caching responses.
API publishers can use API Cloud to monitor API traffic and send alerts when API calls are slow or returning errors.
API Cloud also collects analytics about API traffic and subscriptions.

## Store

https://wso2apistore.services.wisc.edu

The API store allows API subscribers to browse for APIs, interact with documentation, and create an application to integrate with an API.
API subscribers can create applications, which are a collection of subscriptions to one or many APIs.
Each application has unique API keys which allow an API subscriber to access APIs via OAuth2.
For more information on subscribing to APIs and the API Store, [see this article](../../../subscribe-to-apis).

## Gateway

https://gateway.api.wisc.edu

The API gateway proxies all API traffic between client applications and backend APIs.
The API gateway uses OAuth2 to manage tokens and authorize access to backend API resources.
Other features such as throttling, quotas, and caching are enforced through the API gateway to protect backend APIs.
All API calls are routed through the gateway URL including OAuth2 token requests and requests to any backend API.

Backend APIs are namespaced in the root resource of the gateway URL.
This is referred to as the “Context” when publishing an API.
For example, https://gateway.api.wisc.edu/foo and https://gateway.api.wisc.edu/bar could be proxied to two completely different backend APIs, each with different authorization, quota, and throttling mechanisms.
Namespacing helps ensure that multiple API publishers can use the gateway without interfering with each other during runtime.
