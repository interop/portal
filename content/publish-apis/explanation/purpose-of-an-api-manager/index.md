---
title: "Purpose of an API Manager"
date: 2019-11-20T13:24:00-06:00
draft: false
weight: 1
---

WSO2 API Cloud is one of many API Management products available. We chose WSO2 API Cloud as our API manager because it allowed for an easier transition away from our on-premise WSO2 software.
By using an API Manager that is provided to us as Software as a Service (SaaS), we can offload the burden of maintenance, scaling, upgrades, and operations to the vendor.

If you are a provider of APIs, an API Manager can help by providing tooling to configure features that are common among most APIs.
For example, instead of all APIs implementing their own OAuth2 server or other authorization mechanism, you can use the API Manager to handle OAuth2 credentials and tokens for you.
API Managers also help protect your backend API from excessive use or sudden spikes in traffic, including protection against Denial of Service (DoS) attacks.

API Managers also help provide self-service interfaces so that API subscribers can interact with documentation without needing to contact API providers directly, and API providers don’t need to manage a separate documentation environment for their API.
