---
title: "Restricting Publisher Access to Edit APIs"
date: 2019-12-02T17:05:00-06:00
draft: false
---

When getting access to the API Publisher, you will receive links to two Manifest groups that allow you to (1) control who can edit your API and (2) control who can use your API.
This how-to guide will show you how to control who is able to edit your API.

To control who is able to use your API, [follow this how-to guide](../restricting-subscriber-access).

You have access to edit the Manifest groups to control membership.
We recommend creating a separate Manifest group or using an existing group instead of adding individual people directly to the groups that we provide.
In other words, we recommend that you add your group as a member of the group that we provide to you.
For more information and on using Manifest, [see this documentation](https://kb.wisc.edu/page.php?id=27796).

To begin restricting publisher access to your API, go to the [API Publisher](https://publisher.api.wisc.edu) and edit an existing API or create a new API.
For more information on publishing an API, [follow this tutorial](../../tutorials/design-and-publish-api).

In the first step ("Design") of publishing or editing the API, choose "Restricted by roles" in the "Access Control" dropdown menu.
A text field will appear ("Visible to Roles").
In the text field, add the name of the group that was sent to you (I.E. the group that ends with `_api_publisher`) with the prefix `Internal/` (case-sensitive).
For example, if the publisher group is `foo_api_publisher`, enter the text `Internal/foo_api_publisher` in the "Restricted by roles" text field.

![add-access-control-role](./images/add-access-control-role.png)

At this point, save the API or continue designing if this is a new API.
Your API will now only be editable by people within the publisher Manifest group.
