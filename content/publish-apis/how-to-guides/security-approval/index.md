---
title: "Security Approval for Publishing APIs"
date: 2019-11-22T13:00:00-06:00
draft: false
---

Publishing an API involves considering what kind of data it exposes.
Use this guide to understand the necessary steps to take before publishing and before letting a user subscribe to your API.
In some cases, approval from DoIT Cybersecurity (“Cybersecurity”) is necessary before publishing an API or subscribing to an API.
In such cases, please contact Cybersecurity to initiate the approval process by sending an email to: grc-cybersecurity@cio.wisc.edu

Each section in this guide is categorized by the four data classifications.
For more information on the data classifications and the data elements that fall into each category, see this article.
Your API might have different data classifications depending on the resource.
For example, the “/directory” resource might only contain public data while the “/employees” resource might contain sensitive data.
In that case, follow this guide using the highest data classification your API exposes.

||Requires Cybersecurity Approval for **Publishing**|Requires Cybersecurity Approval for **Subscribing**|
|---|---|---|
|Public Data|No|No|
|Internal Data|No|Yes|
|Sensitive Data|Yes|Yes|
|Restricted Data|Yes|Yes|

## Public Data

Public data APIs require no Cybersecurity approval to publish and no approval to subscribe to the API.
API calls will be authenticated, but there is no need to tell API subscribers to contact Cybersecurity for approval before using the API.
All API subscribers can register for your API in a self-service manner.
For API Cloud, this means that there is no need to set scopes for any public data API resources.
For more information on scopes, see this [how-to guide](../../how-to-guides/restricting-subscriber-access).

## Internal Data

Internal data APIs require no Cybersecurity approval to publish.
However, Cybersecurity approval is required before authorizing a subscriber to access your API.
Cybersecurity needs to confirm that the API subscriber won’t expose any internal data to the public in their application.

## Sensitive Data

APIs that expose sensitive data need Cybersecurity approval before publishing, and then each subscription to the API also requires Cybersecurity approval.

## Restricted Data

APIs that expose restricted data need Cybersecurity approval before publishing, and then each subscription to the API will require Cybersecurity approval.
