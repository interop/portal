---
title: "Enable Mutual TLS With a Backend API"
date: 2019-11-22T13:05:00-06:00
draft: false
---

With mutual TLS (also known as mutual SSL, two-way TLS, and two-way SSL), the API Cloud and the backend API share certificates.
This allows the backend API to know that requests being proxied to it are digitally signed by API Cloud.
Mutual TLS provides an additional layer of security beyond allowing the API Cloud IP addresses.
For more information on mutual TLS, [see this article](https://medium.com/sitewards/the-magic-of-tls-x509-and-mutual-authentication-explained-b2162dec4401).

To enable mutual TLS with your API, please send an email to integration-platform@doit.wisc.edu and include the following:

* The name of your API.
* The hostname of your backend API.
* The public certificate for your backend API.

We will work with the vendor (WSO2) to enable mutual TLS for your API.
We will also provide the public certificate for API Cloud.
