---
title: "Basic or Digest Authentication for Backend APIs"
date: 2019-11-22T13:17:00-06:00
draft: false
---

This guide will show how to enable basic or digest authentication for your backend API.
For more information on basic or digest authentication, [see this explanation](../../explanation/securing-backend-apis).
You can add basic or digest authentication during API publishing or by editing the API after it’s published.

In the “Implement” stage of publishing or editing an API, click “Show More Options”.

![Endpoints](./images/endpoints.png)

For “Endpoint Security Scheme”, choose “Secured”. This will make some additional fields appear.
For “Endpoint Auth Type”, choose either “Basic Auth” or “Digest Auth”, depending on what your backend API support.
In the “Credentials” fields, enter the username and password used to authenticate requests to your backend API.

![Credentials](./images/credentials.png)

Finally, click save at the bottom of the page.
Requests to your backend API will now use the authentication method you chose (basic or digest) with the credentials you provided.
