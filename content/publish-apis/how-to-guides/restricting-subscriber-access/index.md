---
title: "Restricting Subscriber Access to APIs"
date: 2019-11-22T13:07:00-06:00
draft: false
---

This guide illustrates two methods for restricting how subscribers can use your API: Using OAuth scopes and restricting API visibility in the API Store.
When you initially get access to the publisher, you will receive a link to a Manifest group that allows you to manage who can access and see your API.
The group name ends with `_api_subscribe`.
This group is used in both methods for restricting subscriber access to your API.

## Restricting API Access With OAuth Scopes

When publishing or editing an API, you can create scopes which map to the Manifest group that was sent to you when getting access to the API Publisher.
Then you can associate the scope with your API resources.

This method will restrict who is able to use your API, but it won't control the visibility of your API documentation or a person's ability to subscribe to your API.
Subscribing to an API doesn't mean that the person has access to use it.
By using OAuth scopes, you can control who is able to use your API *after* subscribing to it.

The words “group” and “role” will be used interchangeably for the purposes of this how-to guide and the screens displayed in the API Publisher.

*Note: If you require groups for additional APIs, or require more groups for fine grained access control (e.g. read-only access and read/write access), contact us (integration-platform@doit.wisc.edu) and we can work to facilitate your use case.*

During the third step of publishing or editing an API (the “Manage” step), go to the bottom of the page under the heading “Resources”.

![Resources](./images/resources.png)

Click “Add Scopes”.

* Scope Key: This should be a short phrase that uniquely identifies the scope for your API.
* Scope Name: This should be a short, human-friendly name to describe the scope.
* Roles: This is where you indicate what group should be able to use this scope.
Add the prefix “Internal/” (without the quotes, case-sensitive) to your group.
For example, if your group name is “foo_api_subscribe”, enter “Internal/foo_api_subscribe” as the role.
* Description: A description to indicate what access this scope permits.

![Define scope](./images/define-scope.png)

Click “Add Scope”. Next, click “+ Scope” next to the resource that you would like to protect.

![UUID resource](./images/uuid-resource.png)

Select the scope you created from the drop-down menu and click the check mark button.

![Get a UUID scope](./images/get-a-uuid-scope.png)

Click “Save & Publish at the bottom of the page.

![Save and publish](./images/save-and-publish.png)

At this point, only members of the group “foo_api_subscribe” will be able to use the “/uuid” resource.

*Note: If someone subscribes to an API but isn’t in the group that is restricting the API resources, they will receive an error when trying to use the API.
After adding them to the group in Manifest, they will need to login to the API Store again so that the updated group associations can be synced to WSO2 API Cloud.
They will also need to regenerate an OAuth token.*

### A note on security

Unless your API is just exposing public data, you are required to use OAuth scopes to restrict who is able to use your API.
This is because DoIT Cybersecurity approval is required before someone can use your API (unless it's just exposing public data).
For more information on the controls required for each data classification, [follow this how-to guide](../security-approval).

## Restricting API Visibility

API Cloud has three options for API visibility in the API store: Public, Visible to my Domain, and Restricted by roles.
These options can be set in the first step ("Design") of publishing or editing an API.
Use the "Visibility on Store" dropdown menu to select your desired visibility option.
We recommend the visibility to be public as a default.

### Public

Setting the visibility to public will allow anyone to be able to view your documentation. Anyone who has access to login to the API Store will be able to subscribe to your API. API documentation is able to be viewed anonymously.

### Visible to my Domain

"Visible to my Domain" allows anyone to view and subscribe to your API if they have access to the API Store. This is similar to Public, except that users unauthorized to use the API Store will not be able to see your API documentation.

### Restricted by roles

Restricting API visibility by roles allows you to use a Manifest group to control the specific people that are able to view and subscribe to your API.

Restricting API visibility won't affect existing subscriptions to your API.
For example, consider the scenario where you published an API with public visibility and someone subscribed and started using the API.
Changing the visibility will exclude the person from being able to view and create new subscriptions to your API, but it won't affect their existing subscription to your API and they will be able to continue using the API.

In the first step ("Design") of publishing or editing an API, choose "Restricted by roles" in the "Visibility on Store" dropdown menu.
A text field will appear "Visible to Roles".
In the text field, add the name of the group that was sent to you (I.E. the group that ends with `_api_subscribe`) with the prefix `Internal/` (case-sensitive).
For example, if the subscriber group is `foo_api_subscribe`, enter the text `Internal/foo_api_subscribe` in the "Restricted by roles" text field.

![Visibility](./images/visibility.png)

At this point, save the API or continue designing if this is a new API.
Your API will now only be visible and subscribe-able by people within the subscriber Manifest group.