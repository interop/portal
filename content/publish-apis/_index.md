---
title: WSO2 API Publisher
date: 2019-11-25T15:05:14-06:00
---

API Cloud is an API management product provided by WSO2, and it is the current API manager for UW-Madison.
API managers are used to help centralize API documentation, client registration, and usage analytics.
For more information on API management, [see this article](https://en.wikipedia.org/wiki/API_management).
For more information on APIs, [see this article](https://www.freecodecamp.org/news/what-is-an-api-in-english-please-b880a3214a82/).

![API cloud overview](./images/api-cloud-overview.png)

API publishers use API Cloud to share information about their APIs including how to use it, steps for getting access, and provide the URL to their API to allow API Cloud to proxy traffic through its gateway.
By allowing API Cloud to serve as a proxy, it can enforce policies to protect APIs as well as collect analytics from API traffic.
Additionally, API Cloud can handle client authorization using OAuth2. OAuth2 is a widely adopted standard for API authorization.
For more information on OAuth2, [see this article](https://blogvaronis2.wpengine.com/what-is-oauth/).

After an API is published to API Cloud, one or multiple API subscribers can register an application to use the API.
Visibility to API documentation and permission to access API resources can be restricted.
API Cloud integrates with [Manifest](https://kb.wisc.edu/helpdesk/27796) to assist with API publisher and subscriber authorization.

[Please follow these instructions to get access to publish APIs](./reference/get-access).

For more information about using API Cloud as a client/subscriber, [see this document](../subscribe-to-apis).
