---
title: "Design and Publish an API"
date: 2019-11-20T16:45:00-06:00
draft: false
weight: 1
---

One of the main features of API Cloud is the ability to design and publish an API. In this tutorial, we will walk you through the process of designing and publishing an API.
The API will interact with https://httpbin.org as the backend API.
API Cloud will proxy API calls through its gateway. When we test the API we create, we will send requests through API Cloud which will be routed to https://httpbin.org.
The API we create will return a random UUID.

Before starting this tutorial, you will need access to the API Publisher.
Please [follow these instructions to get access to the API publisher](../../reference/get-access).

Log in to the publisher with your NetID and password: https://publisher.api.wisc.edu

After logging in, you will arrive at the home page for the publisher. Click “Add New API”:

![Add a new API](./images/add-a-new-api.png)

Click “Design a New REST API”, then click “Start Creating”.
REST is an architectural style most commonly used in web service APIs.
It involves categorizing data into resources and interacting with resources using common HTTP verbs such as GET, PUT, POST, and DELETE.
For more information on REST, [see this article](https://medium.com/extend/what-is-rest-a-simple-explanation-for-beginners-part-1-introduction-b4a072f8740f).

![Design a new REST API](./images/design-new-rest-api.png)

## Design

Fill in the general details for the API:

* Name: “interop-tutorial” (feel free to change this to a custom name)
	* This is the name that will appear in the API publisher and API store.
* Context: “interop” (feel free to change this to something custom)
	* This is the name that will appear in the URL for subscribers that call your API.
The context should be unique but be readable enough for API subscribers to know which API they’re interacting with.
For example, if the context is “foo”, the URL will begin with “https://gateway.api.wisc.edu/foo”
* Version: “v1”
* Access Control: choose “Restricted by roles”.
A text box will appear (Visible to Roles) to add a list of groups.
Add the name of the group that was sent to you when you got access to the API publisher (I.E. the group that ends with `_api_publisher`) with the prefix `Internal/` (case-sensitive).
By using this group, your API will only be editable by people in the group.
* Visibility on Store: choose “Restricted by roles”.
A text box will appear (Visible to Roles) to add a list of groups.
Add the name of the group that was sent to you when you got access to the API publisher (I.E. the group that ends with `_api_subscribe`) with the prefix `Internal/` (case-sensitive).
This will make sure other people outside of your group can’t see or subscribe to your API in the API store.
* Description: Add a description. This should help give API subscribers an overview of the purpose of your API.

![Design details](./images/design-details.png)

In the “URL Pattern” section, enter “uuid”, select the box for “GET”, and click “Add”.
We’ve now created the uuid resource. Enter a short summary for the uuid resource.

Next, click on “/uuid” (next to “GET”) to expand some additional options for that resource.
For the “Produces” field, enter “application/json”.
This will allow API subscribers to know what kind of data your API produces.
Other common types are application/xml, text/plain, and text/csv.

![API resources](./images/api-resources.png)

At this point, you have designed an API by providing general information, base URL structure, and a “uuid” resource that will later allow us to get a random UUID using the HTTP GET method.
Click “Next: Implement”.

## Implement

Click “Managed API”.

For the “Endpoint Type”, choose “HTTP/REST Endpoint”. For the Production endpoint, use this URL: https://httpbin.org

Click “Test” to make sure the URL is valid.

Leave the Sandbox endpoint blank.

![Endpoints](./images/endpoints.png)

Click “Next: Manage”.

## Manage

In “Configurations”, check the box “Make this the default version”.
This will allow API subscribers to call your API without putting the version in the URL.

![Make default version](./images/make-default-version.png)

In “Throttling Settings”, check the box “Unlimited” for the purposes of this tutorial.

![Subscription tiers](./images/subscription-tiers.png)

At the bottom of the page, click “Save and Publish”.
A window will appear to confirm your API is published. Click “Go to API Store”.

![Congratulations... what's next?](./images/congratulations-whats-next.png)

## Subscribe in the API Store

After going to the API store, you might see an error that your API doesn’t exist or you aren’t authorized to view it.
This means that you aren’t logged in yet.

![No API available error](./images/no-api-available-error.png)

Click “Sign In” at the top of the page.
If you aren’t automatically logged in, you will be prompted to sign in with your NetID and password.

![Sign in](./images/sign-in.png)

Search for the name of the API you created and click on the API.

![Search](./images/search.png)

To subscribe to the API, choose “DefaultApplication” in the Applications dropdown, and choose “Unlimited” in the Tiers dropdown. Click Subscribe.

![Subscribe](./images/subscribe.png)

Click “View Subscriptions” in the window that appears after subscribing to the API.

![Subscription successful](./images/subscription-successful.png)

You will now see the applications that the DefaultApplication is subscribed to.

![No keys found](./images/no-keys-found.png)

Next, click “Production Keys” and then click “Generate keys” at the bottom of the page.
This will generate a consumer key and consumer secret.
**The consumer secret should be treated like a password and be kept secret.**
Generating keys will also generate an access token that we will use in the next steps, but there is no need to remember or save it anywhere.
**Tokens should also be treated like keys and be kept secret.**
Keys, tokens, and grant types all relate to the OAuth2 authorization standard.
For more information on OAuth2, [see this article](https://blogvaronis2.wpengine.com/what-is-oauth/).

*Note: Tokens expire after a set duration of time.
3600 seconds (1 hour) is the default.
If you pause this tutorial after generating the token, you might need to go back into your DefaultApplication and click “Regenerate” before proceeding with the next steps.*

Now you will try using the API you just published. Click on “Subscriptions”, and then click on the API you published and subscribed to.

![Subscriptions](./images/subscriptions.png)

Click on “API Console”.
The API store should automatically populate your access token based on the subscription in your DefaultApplication.
If it isn’t populated choose your DefaultApplication from “Try” choose Production from “Using”.
“Set Request Header” should populate your access token.

![API console](./images/api-console.png)

Click on “GET /uuid” to expand details about that resource. Click “Try it out”.

![Get ](./images/get-uuid.png)

Click “Execute”. This will initiate the API call to your uuid API.

![Execute](./images/execute.png)

When the API call is completed, you will see the response body in the “Server Response” section which will contain a JSON payload with a random uuid.

![API response](./images/api-response.png)

## Summary

In summary, we went through the process of designing and publishing an API, then using the API store to subscribe to the API.
Next, we generated OAuth2 keys and an OAuth2 token, and used the token to make an API call within your web browser.
