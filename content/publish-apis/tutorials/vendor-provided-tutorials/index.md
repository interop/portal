---
title: "Vendor-Provided Tutorials"
date: 2019-11-22T12:54:00-06:00
draft: false
weight: 2
---

The vendor (WSO2) provides tutorials related to API publishing: https://docs.wso2.com/display/APICloud/API+Publishing

When you are instructed to go to or the API publisher, use this URL: https://publisher.api.wisc.edu

Some tutorials may instruct you to contact WSO2 directly to enable a certain feature.
Instead, please email us (integration-platform@doit.wisc.edu) so that we can facilitate communication with the vendor.
